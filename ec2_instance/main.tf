provider "aws" {
  region     = "us-east-1"
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
    lock_address   = "https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}

module "ec2_instance" {
  source = "../modules/ec2-module"
  instance_type = "t3.medium"
  sg_att_id = module.sg.output_sg_id
  aws_ec2_tag = {
    Name = "odoo-aCD-ec2"
  }
}

module "ebs_volume" {
  source       = "../modules/ebs-module"
  az_ebs       = module.ec2_instance.ec2_az_output  
  size_ebs     = 100
  type_ebs     = "gp2"  
  name_ebs     = "odoo-aCD-ebs"
  ec2_id_ebs   = module.ec2_instance.ec2_id_output 
  device_name_ebs = "/dev/sdf"
}

module "eip_address" {
  source = "../modules/eip-module"
  eip_name = "odoo-aCD-eip"
  ec2_id = module.ec2_instance.ec2_id_output
}

resource "null_resource" "export_info" {
    provisioner "local-exec" {
    command = "echo 'ec2_info:\n  server_public_ip: ${module.eip_address.eip_output}' > public_ip.yml"
  }  
}

module "sg" {
  source = "../modules/sg-module"
  name_sg = "odoo-aCD-sg"
}
