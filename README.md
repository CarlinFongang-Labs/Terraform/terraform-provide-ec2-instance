# Terrarm | Ce manifest permet de Provisionnement une infrastructure AWS [ec2, EBS, SG, EIP] et de récuperer l'ip de l'instance cible provisionnée et utilisant S3 ou Gitlab comme stockage pour le remote management

_______



><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Indications
Ce code terraform permet de provisionner 1 instances ec2 basique sous ubuntu 22.04 LTS (ec2_target_host) et un une instance ec2 sous ubuntu 22.04 LTS avec Ansible préalablement installé (ec2_ansible)

### 0. Créer un bucket pour le stockage du "tfstate" 

Créer un bucket s3 nommé **backend-acd** où sera stocké le fichier **tfstate** qui permettra de décrire à tout instant l'état de l'infrastrucutre aws.


### 1. Pour provisionner une instance ec2

#### 1. 

````bash
cd terraform-provide-ec2-instance/ec2_target_host
````
Ajuter la valeur des variables **`instance_type, size_ebs`** dans le fichier **main.tf** au besoin 

#### 2. 

````bash
export AWS_ACCESS_KEY_ID='AKIA6GBMBEWYHFU6NQM3'
export AWS_SECRET_ACCESS_KEY='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'
export AWS_SESSION_TOKEN='550e8400-e29b-41d4-a716-446655440079'
````

#### 3. 
Copier la clé ssh générée à partir d'AWS dans le repertoire `terraform-provide-ec2-instance/secret/`

#### 4. 

````bash
cd terraform-provide-ec2-instance/ec2_target_host
terraform init -reconfigure
terraform plan
terraform apply -auto-approve
````
#### 5. Récupérer l'eip de l'intance
dans le repètoire `terraform-provide-ec2-instance/secret/`, se trouve le fichier `info_client_aCD.txt` dans lequel est sauvegardé l'élastic IP de l'instance qui a été provisionnée.

Enjoy, l'instance est à présent prête à l'emploi 🥳🎉🎉

Noter qu'en ajustant le module `modules/ec2-ansible/main.yml` et en y renseignant un fichier bash custumisé (`script = "../ansible_install.sh"`), il es possible alors de provisionner et configurer l'instance grace au fichier bash personnalisé.

le code à modifier dans ce module est le provisioner ci-dessous 

````yaml
provisioner "remote-exec" {
    script = "../ansible_install.sh"
  connection {
    type = "ssh"
    user = "ubuntu"
    private_key = file("../secret/devops-aCD.pem")
    host = self.public_ip
    }
  }
````

#=======

## Remote state avec Gitlab

### 1. Modifier le fichier `ec2_docker/main.tf`

```yaml
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
    lock_address   = "https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${TF_STATE_NAME}/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
  }
}
```

### 2. Export des informations d'authentification

```bash
export GITLAB_ACCESS_TOKEN=''
export TF_STATE_NAME='docker_state'
export PROJECT_ID='57006050'
```

### 3. Initialisation de terraform
````bash
rm -rf .terraform
````

```yaml
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=CarlinFongang" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

>![alt text](img/image.png)
*Initiation de terraform*

### 4. Exporter les infos d'auth aws

````bash
export AWS_ACCESS_KEY_ID='AKIAXYKJWQ4Z3LLY4TEN'
export AWS_SECRET_ACCESS_KEY='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'
````

### 5. Plan

```bash
terraform plan
```

>![alt text](img/image-1.png)

### 6. Apply

```bash
terraform apply -auto-approve
```

>![alt text](img/image-2.png)

![alt text](img/image-3.png)