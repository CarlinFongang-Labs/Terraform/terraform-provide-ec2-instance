#!/usr/bin/env bash
# Install ansible
sleep 5
sudo apt update -y
sudo apt install software-properties-common
sudo apt install python3-pip -y
sudo pip3 install ansible
#sudo apt-add-repository --yes --update ppa:ansible/ansible

sleep 5
sudo apt install ansible -y


## 1. Package update
sudo apt list --upgradable
sudo apt update -y


## 2. Installation of dependencies for Docker
# sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

# ## 3. Adding the official Docker GPG key
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# ## 4. Adding the Docker repository to APT sources
# echo "" | sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

# ## 6. Docker installation
# sudo apt install -y docker-ce

# ## 7. Checking the status of Docker
# #sudo systemctl status docker

# ## 8. Adding the user to the Docker group
# sudo usermod -aG docker $USER
# sudo systemctl start docker
# sudo systemctl enable docker

# ## 9. Git installation
# sudo apt install -y git

# sleep 5

# #Install docker-compose
# sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose


# ## Téléchargement de AWX
# # Clone du dépôt
# git clone https://gitlab.com/CarlinFongang-Labs/Ansible/tower.tar.gz.git

# # Changement de répertoire
# cd tower.tar.gz/

# # Extraction de l'archive dans le répertoire de destination
# tar xvf awx.tar.gz -C ~/
# sudo apt update -y

# ## Installation de AWX
# #docker-compose up -d
